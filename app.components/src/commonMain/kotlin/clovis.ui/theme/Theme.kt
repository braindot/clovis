package clovis.ui.theme

import clovis.ui.theme.color.Palette

/**
 * The overall UI theme of the app.
 */
data class Theme(

	/**
	 * Colors used by the app.
	 */
	val palette: Palette,
)
